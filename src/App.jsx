import './App.css';
import Header from "./components/Header/Header";
import LeftNavBar from "./components/LeftNavBar/LeftNavBar";
import Content from "./components/Content/Content";


function App() {
    return (
        <div className="App">
            <Header/>
            <div className="Content-Wrapper">
                <LeftNavBar/>
                <Content/>
            </div>
        </div>
  );
}

export default App;
