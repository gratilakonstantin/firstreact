import React, {Component} from 'react';
import './styles.css';

class Content extends Component {
    render() {
        return (
            <div className="App-Content">
                <h1>Content</h1>
            </div>
        );
    }
}

export default Content;